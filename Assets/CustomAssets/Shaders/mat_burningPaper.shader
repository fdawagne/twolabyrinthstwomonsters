﻿Shader "Custom/mat_burningPaper" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_TransitionTex("Transition texture (G)", 2D) = "white" {}
		_Transition("Transition", Range(-0.3, 1.2)) = 0.5
	}
		SubShader
		{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows addshadow
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _TransitionTex;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_TransitionTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _Transition;

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			float limit = tex2D(_TransitionTex, IN.uv_TransitionTex).g;

			clip(limit - _Transition);

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb * smoothstep(0.1f, 0.2f, limit - _Transition) ;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;

			o.Emission = smoothstep(0.3f, 0.2f, limit - _Transition) * float3(2.0f, 0.2f, 0.0f) * smoothstep(0.1f, 0.2f, limit - _Transition);
		}
		ENDCG
		}
			FallBack "Diffuse"
}
