﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInterraction : MonoBehaviour
{

    [SerializeField]
    private Camera pointOfView;

    private GameObject camTarget;

    private Vector3 eyePosition;
    private Vector3 lookTowards;
    private float lookDistance = 1.5f;


    private bool[] inventory;

    private void Awake()
    {
        int i = 0;
        foreach (PickUp item in FindObjectsOfType<PickUp>())
        {
            item.objID = i;
            i++;
        }
        Debug.Log(i + " items to pick up");
        inventory = new bool[i];
    }


    void Start()
    {
        Debug.Log("Player instantiated");
    }

    void Update()
    {
        RaycastHit hit;

        eyePosition = pointOfView.transform.position + pointOfView.transform.forward * 0.4f;
        lookTowards = pointOfView.transform.forward;

        Color colRay = Color.red;

        if (Physics.Raycast(eyePosition, lookTowards, out hit, lookDistance) && hit.collider.tag == "Interractable")
        {
            GameObject objLookedAt = hit.collider.gameObject;

            colRay = Color.green;

            objLookedAt.GetComponent<Interractables>().StartGlowing();

            if (Input.GetKeyDown(KeyCode.E))
            {
                objLookedAt.GetComponent<Interractables>().Interract(gameObject);
            }

            camTarget = objLookedAt;
        }
        else if (camTarget != null)
        {
            camTarget.GetComponent<Interractables>().StopGlowing();
            camTarget = null;
        }

        Debug.DrawRay(eyePosition, lookTowards * lookDistance, colRay);
    }

    public bool hasObject(int objID)
    {
        return inventory[objID];
    }

    public void addObject(int objID)
    {
        inventory[objID] = true;
    }

    public void removeObject(int objID)
    {
        inventory[objID] = false;
    }

}
