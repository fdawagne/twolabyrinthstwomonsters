﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    [Range(0, 100)]
    public int hp = 100;
    [Range(0, 100)]
    public int hunger = 100;
    [Range(0, 100)]
    public int thirst = 100;

    // Use this for initialization
    void Awake()
    {
        hp = 100;
        hunger = 100;
        thirst = 100;
    }

    // Update is called once per frame
    //void Update()
    //{

    //}

    void Drink(int value)
    {
        thirst += value;
    }

    void Eat(int value)
    {
        hunger += value;
    }

    void Heal(int value)
    {
        hp += value;
    }
    void takeDamages(int value)
    {

    }
}
