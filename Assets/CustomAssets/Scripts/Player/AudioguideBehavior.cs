﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AudioguideBehavior : MonoBehaviour
{
    public int CheatPlayNumber;
    private AudioSource theSource;
    private AudioClip[] audioTab = new AudioClip[100];
    private AudioClip demonicSound;

    private Text subtitles;
    private string[] stringTab;

    private void Awake()
    {
        LoadRessources();
        subtitles = GameObject.FindGameObjectWithTag("Subtitles").GetComponent<Text>(); ;
        SetSubtitles();
    }

    public void LoadRessources()
    {
        theSource = GetComponent<AudioSource>();

        for (int i = 0; i < audioTab.Length; i++)
        {
            audioTab[i] = (AudioClip)Resources.Load("AG/AG_" + i, typeof(AudioClip));
        }
        demonicSound = (AudioClip)Resources.Load("AG/AG_demon", typeof(AudioClip));

        stringTab = File.ReadAllLines(Application.dataPath + "/CustomAssets/Resources/AG/AG_subtitlesFR.txt");
    }

    public void PlayAgNumber(int nbr)
    {
        if (nbr < audioTab.Length && audioTab[nbr] != null)
        {
            theSource.clip = audioTab[nbr];
            SetSubtitles(nbr);
        }
        else
        {
            theSource.clip = demonicSound;
        }
        theSource.Play();

        Invoke("SetSubtitles", theSource.clip.length);
    }

    private void SetSubtitles()
    {
        subtitles.text = "";
    }

    private void SetSubtitles(string message)
    {
        subtitles.text = message;
    }

    private void SetSubtitles(int nbr)
    {
        subtitles.text = stringTab[nbr];
    }
}
