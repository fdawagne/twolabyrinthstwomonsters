﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapBehavior : MonoBehaviour
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("mapOpen", Input.GetKey(KeyCode.Tab));
    }
}
