﻿using UnityEngine;
using System.Collections;

public class MonsterBehavior : MonoBehaviour
{
    private GameObject player;
    private UnityEngine.AI.NavMeshAgent nav;
    public float FieldOfView = 5f;
    private float walkSpeed = 3.5f;
    private float runSpeed = 7f;

    // Use this for initialization
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        nav.speed = walkSpeed;
        RaycastHit hit;
        Color col = Color.red;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, FieldOfView) && hit.collider.gameObject == player)
        {
            col = Color.green;
            nav.speed = runSpeed;
        }
        nav.SetDestination(player.transform.position);
        Debug.DrawRay(this.transform.position, this.transform.forward * FieldOfView, col);
    }
}
