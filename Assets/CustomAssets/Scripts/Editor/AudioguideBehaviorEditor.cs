﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AudioguideBehavior))]
public class AudioguideBehaviorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Load sounds"))
        {
            (target as AudioguideBehavior).LoadRessources();
        }
        if (GUILayout.Button("play audio"))
        {
            (target as AudioguideBehavior).PlayAgNumber((target as AudioguideBehavior).CheatPlayNumber);
        }
    }

}
