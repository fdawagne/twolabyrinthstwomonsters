﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consume : MonoBehaviour
{
    private float transition = -0.1f;
    private bool animate = false;

    private Material mat;

    private void Awake()
    {
        mat = Instantiate(GetComponent<MeshRenderer>().sharedMaterial);
        GetComponent<MeshRenderer>().material = mat;
    }

    private void Update()
    {
        if (animate)
        {
            transition += 1/80f;
            if (transition >= 1.1)
            {
                transition = 1.1f;
                animate = false;
            }

            mat.SetFloat("_Transition", transition);
        }
    }

    public void ConsumeObject()
    {
        Debug.Log("Consume paper");
        transition = -0.1f;
        animate = true;
    }

}
