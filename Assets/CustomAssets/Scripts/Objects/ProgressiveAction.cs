﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressiveAction : Interractables
{
    [SerializeField]
    private PickUp keyObj;
    private int keyObjID;

    private Animator anim;
    private AudioSource sfx;

    private void Start()
    {
        Initiallize();
        anim = GetComponent<Animator>();
        sfx = GetComponent<AudioSource>();
        if (keyObj != null)
        {
            keyObjID = keyObj.objID;
        }
    }

    // Update is called once per frame
    public override void Interract(GameObject whoDidIt = null)
    {
        if (whoDidIt.CompareTag("Player"))
        {
            if (whoDidIt.GetComponent<PlayerInterraction>().hasObject(keyObjID))
            {
                anim.SetTrigger("addKeyItem");
                whoDidIt.GetComponent<PlayerInterraction>().removeObject(keyObjID);
            }
            else 
            {
                anim.SetTrigger("startTurning");
            }
        }
    }

    private void Update()
    {
        
    }

    public void ActionIsFinished()
    {
        anim.SetBool("finished", true);
    }

    public void playSound()
    {
        sfx.Play();
    }

}
