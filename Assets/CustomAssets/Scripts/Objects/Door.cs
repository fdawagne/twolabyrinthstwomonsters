﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interractables
{

    [SerializeField]
    private PickUp key;
    private int keyID;
    private bool needKey;

    private Animator anim;

    private AudioSource sfx;

    [SerializeField]
    private AudioClip wriggleSound;
    [SerializeField]
    private AudioClip unlockSound;


    public void Start()
    {
        anim = GetComponent<Animator>();
        sfx = GetComponent<AudioSource>();
        needKey = (key != null);
        anim.SetBool("needKey", needKey);
        if (needKey)
        {
            keyID = key.objID;
        }
        Initiallize();
    }

    public override void Interract(GameObject whoDidIt = null)
    {
        Open(whoDidIt);
    }

    private void Open(GameObject whoDidIt)
    {
        if (whoDidIt.CompareTag("Player"))
        {
            if (needKey && whoDidIt.GetComponent<PlayerInterraction>().hasObject(keyID))
            {
                anim.SetBool("hasKey", true);
                whoDidIt.GetComponent<PlayerInterraction>().removeObject(keyID);
            }
            else
            {
                anim.SetTrigger("tryToOpen");
            }
        }
    }

    private void WriggleSound()
    {
        sfx.clip = wriggleSound;
        sfx.Play();
    }

    private void UnlockSound()
    {
        sfx.clip = unlockSound;
        sfx.Play();
    }

    void Untag()
    {
        gameObject.tag = "Untagged";
    }
}
