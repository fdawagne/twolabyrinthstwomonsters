﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : Interractables
{
    public int objID;

    public override void Interract(GameObject whoDidIt = null)
    {
        if (whoDidIt.tag == "Player")
        {
            getsPickedUp(whoDidIt);
        }
    }

    private void getsPickedUp(GameObject whoDidIt)
    {
        whoDidIt.GetComponent<PlayerInterraction>().addObject(objID);
        Destroy(gameObject);
    }
}
