﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crackedDoor : Interractables
{

    private BoxCollider bc;

    private void Start()
    {
        Initiallize();
        bc = GetComponent<BoxCollider>();
    }

    public override void Interract(GameObject whoDidIt = null)
    {
        if (whoDidIt.CompareTag("Player"))
        {
            SlipTrough(whoDidIt);
        }
    }

    private void SlipTrough(GameObject player)
    {
        // begin player transition
        bc.enabled = false;

        // SOULD BE REPLACED BY A PRETTY MOVEMENT BUT I'M TIRED SO NOPE
        float tmpX = player.transform.position.x + 2f * (gameObject.transform.position.x - player.transform.position.x);
        float tmpY = player.transform.position.y;
        float tmpZ = player.transform.position.z + 2f * (gameObject.transform.position.z - player.transform.position.z);
        player.transform.position = new Vector3(tmpX, tmpY, tmpZ);
           
        // end player movemement
        bc.enabled = true;
    }
}
