﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interractables : MonoBehaviour
{

    private Material[] mats;

    private void Awake()
    {
        mats = (GetComponent<MeshRenderer>().sharedMaterials);

        for (int i = 0; i < mats.Length; i++)
        {

            mats[i] = Instantiate(mats[i]);
            GetComponent<MeshRenderer>().materials[i] = mats[i];
        }

        mats = GetComponent<MeshRenderer>().materials;
    }

    void Start()
    {
        Initiallize();
    }

    public virtual void Interract(GameObject whoDidIt = null)
    {

    }

    protected void Initiallize()
    {
        this.gameObject.tag = "Interractable";
        StopGlowing();
    }

    public void StartGlowing()
    {
        foreach (Material mat in mats)
        {
            mat.SetFloat("_OutlineThickness", 0.5f);
        }
    }

    public void StopGlowing()
    {
        foreach (Material mat in mats)
        {
            mat.SetFloat("_OutlineThickness", 0.0f);
        }
    }

}
